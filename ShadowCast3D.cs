// Inspired by this 3d FOV algorithm:
// https://github.com/CleverRaven/Cataclysm-DDA/blob/eefe407af0da9cc8fddb8599188ca745b25d8b9c/src/shadowcasting.cpp
// Thanks developers for idea and game Cataclysm DDA in general, like it 🤍

using System;
using System.Collections.Generic;
using System.Diagnostics;
//using System.Text;
using System.Threading.Tasks;
using Unity.Mathematics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Varafmar.AdAstra
{
    // TODO: Implement vertical FoV
    // TODO: Implement transparency ranged between 0 and 1
    // TODO: Add view offset in cell of viewer position (its center (0.5, 0.5, 0.5) for now)
    public partial class ShadowCast3D
    {
        // TODO: to parameter
        private const float START_FADE = 0.75F;

        private enum CellStateTransientEvent
        {
            FromEmptyToRoof,
            FromEmptyToWall,

            FromRoofToEmpty,
            FromRoofToWall,

            FromWallToRoof,
            FromWallToEmpty,

            None
        }

        private enum CellState
        {
            Empty,
            Wall,
            Roof
        }

        // We split space to 16 areas with different transforms.
        // Each area is initial frustum. Every step we truncate frustum
        // and scan plane that we got. If we meet block area, we split the visible
        // frustum into up to 4 sub-frustums, this is the view from the viewPoint looking out:
        //
        // +----------------+ <- EndZ
        // |     Upper      |
        // +------+---+-----+ <- UpperOcclusionZY
        // | Left |###| CuR |
        // +------+---+-----+ <- BottomOcclusionZY
        // |     Bottom     |
        // +----------------+ <- StartZ
        // ^                ^
        // |                EndX
        // |
        // StartX
        //
        // With floor/roofs this scheme some more complicated../
        //
        // Note that not 100% realistic FoV. Its not calculating XZ axis, so in fact you'll see
        // less by this alghotithm than in reality. That causes some weird behaviour in some cases,
        // cause we projecting bounds of cube on FoV plane using 4 edges, not all edges of cube (4 edges XY and ZY vs 6
        // XY, ZY and XZ in reality): https://imgur.com/fFA6t0X.
        // So if view point would be ontop of wall, you'll see shadows that not would be appear in real life.
        // Have no idea how handle ZX axis :(

        private enum FrustumPlaneType
        {
            // Mostly uses for debuging, but also used for detect if frustum is
            // already scanned or invalid.
            // Invalid means already scanned frustums or when start X >= end X or start Z >= end Z.
            InvalidOrComplited = 0,

            Initial,

            Left,
            Bottom,
            Upper,
            CurrentRight,
        }

        private InputCellData[] input;
        private int arrayCenterIndex;
        private int3 arrayCenter;
        private Bounds bounds;
        private int3 size;
        private float viewRadius;
        private FrustumPlane[] initialFrustums;

        // Every initial frustum have own separate array for the visibility values ​​of border cells
        // to avoid race condition with other frustum scan tasks
        private KeyValuePair<int, float>[][] edgesOutput;

        public float[] Output { get; private set; }
        public int MidpointsCount { get; set; }

        private static FrustumPlane defaultInitialFrustum = new FrustumPlane()
        {
            TruncateY = 1,
            Type = FrustumPlaneType.Initial,

            // Each frustum starts with start slope (left or bottom plane)
            // with k = ∞ (1/0) and with end slope (right or upper plane) with k = 1 (1/1).
            // Also slope coeff 'k' noted as 'm' in geometry.
            // https://en.wikipedia.org/wiki/Slope
            StartZY = new Slope(0, 1),
            EndZY = new Slope(1, 1),
            StartXY = new Slope(0, 1),
            EndXY = new Slope(1, 1),
        };


        // Dont change id, it's uses in cone visibility
        private static readonly FrustumTransform[] frustumAreaTransforms =
        {
            new FrustumTransform( 0,  1,  1,  0,  1,  0.0F,   45.0F,  0  ),   // Upper ENE    id 0
            new FrustumTransform( 1,  0,  0,  1,  1,  45.0F,  90.0F,  1  ),   // Upper NNE    id 1
            new FrustumTransform(-1,  0,  0,  1,  1,  90.0F,  135.0F, 2  ),   // Upper NNW    id 2
            new FrustumTransform( 0, -1,  1,  0,  1,  135.0F, 180.0F, 3  ),   // Upper WNW    id 3
            new FrustumTransform( 0, -1, -1,  0,  1,  180.0F, 225.0F, 4  ),   // Upper WSW    id 4
            new FrustumTransform(-1,  0,  0, -1,  1,  225.0F, 270.0F, 5  ),   // Upper SSW    id 5
            new FrustumTransform( 1,  0,  0, -1,  1,  270.0F, 315.0F, 6  ),   // Upper SSE    id 6
            new FrustumTransform( 0,  1, -1,  0,  1,  315.0F, 360.0F, 7  ),   // Upper ESE    id 7
                                                                         
            new FrustumTransform( 0,  1,  1,  0, -1,  0.0F,   45.0F,  8  ),   // Bottom ENE   id 8
            new FrustumTransform( 1,  0,  0,  1, -1,  45.0F,  90.0F,  9  ),   // Bottom NNE   id 9
            new FrustumTransform(-1,  0,  0,  1, -1,  90.0F,  135.0F, 10 ),   // Bottom NNW   id 10
            new FrustumTransform( 0, -1,  1,  0, -1,  135.0F, 180.0F, 11 ),   // Bottom WNW   id 11
            new FrustumTransform( 0, -1, -1,  0, -1,  180.0F, 225.0F, 12 ),   // Bottom WSW   id 12
            new FrustumTransform(-1,  0,  0, -1, -1,  225.0F, 270.0F, 13 ),   // Bottom SSW   id 13
            new FrustumTransform( 1,  0,  0, -1, -1,  270.0F, 315.0F, 14 ),   // Bottom SSE   id 14
            new FrustumTransform( 0,  1, -1,  0, -1,  315.0F, 360.0F, 15 )    // Bottom ESE   id 15
        };

        public ShadowCast3D(InputCellData[] input, int3 size, float[] output, int midpointsCount = 0)
        {
            ValidateDimensions(size, output, input);

            MidpointsCount = Mathf.Clamp(midpointsCount, 0, int.MaxValue);

            Output = output;
            this.input = input;
            this.size = size;

            initialFrustums = new FrustumPlane[frustumAreaTransforms.Length];

            int3 halfSize = size / 2;

            arrayCenter = halfSize;
            arrayCenterIndex = Utilites.EncodeInt3(halfSize, size);
            bounds = new Bounds(-halfSize, size);

            InitializeEdgesOutput(halfSize);
        }

        private void ValidateDimensions(int3 size, float[] output, InputCellData[] input)
        {
            if ((size.x % 2 == 0) || (size.y % 2 == 0) || (size.z % 2 == 0))
            {
                throw new ArgumentException("Size dimensions must be odd!", nameof(size));
            }

            int inputArraySize = input.Length;
            int cellsCount = size.x * size.y * size.z;
            if (cellsCount != inputArraySize)
            {
                throw new ArgumentException("Input array size must match with size volume!", nameof(input));
            }

            if (output.Length != input.Length)
            {
                throw new ArgumentException("Output array size must match with input array size!", nameof(output));
            }
        }

        private void InitializeEdgesOutput(int3 halfSize)
        {
            int max = Math.Max(halfSize.y, Math.Max(halfSize.z, halfSize.x));
            edgesOutput = new KeyValuePair<int, float>[frustumAreaTransforms.Length][];

            for (int i = 0; i < edgesOutput.Length; i++)
            {
                edgesOutput[i] = new KeyValuePair<int, float>[(max + 1) * (max + 1) * 4];
                for (int j = 0; j < edgesOutput[i].Length; j++)
                {
                    edgesOutput[i][j] = new KeyValuePair<int, float>(-1, 0);
                }
            }
        }

        public void ComputeVisibility360(float viewRadius)
        {
            for (int i = 0; i < frustumAreaTransforms.Length; i++)
            {
                initialFrustums[i] = defaultInitialFrustum;
                initialFrustums[i].TransformId = i;
            }

            ComputeVisibilityInternal(viewRadius, true);
        }

        private void ComputeVisibilityInternal(float viewRadius, bool cleanup)
        {
            Stopwatch sw = Stopwatch.StartNew();

            this.viewRadius = viewRadius;

            if (cleanup)
            {
                Cleanup();
            }

            Output[arrayCenterIndex] = 1.0F;
            Parallel.For(0, frustumAreaTransforms.Length, ScanInitialFrustum);

            AssignEdgeCellsVisibility();

            if (MidpointsCount != 0)
            {
                FloorMidpointOutput();
            }

            sw.Stop();
            PerfomanceLogger.CalcVisiblity += sw.ElapsedMilliseconds;
        }

        private void FloorMidpointOutput()
        {
            for (int i = 0; i < Output.Length; i++)
            {
                ref float visibility = ref Output[i];
                if (visibility > 0)
                {
                    visibility = Utilites.FloorMidpoint(visibility, MidpointsCount);
                }
            }
        }

        private void AssignEdgeCellsVisibility()
        {
            foreach (var edgeOutput in edgesOutput)
            {
                for (int i = 0; i < edgeOutput.Length; i++)
                {
                    if (edgeOutput[i].Key == -1)
                    {
                        continue;
                    }

                    Output[edgeOutput[i].Key] += edgeOutput[i].Value;
                    edgeOutput[i] = new KeyValuePair<int, float>(-1, 0);
                }
            }
        }

        private void ScanInitialFrustum(int i)
        {
            if (initialFrustums[i].Type != FrustumPlaneType.InvalidOrComplited)
            {
                ScanFrustum(initialFrustums[i]);
            }
        }

        private void Cleanup()
        {
            // Edges output cleaning in AssignEdgeCellsVisibility
            for (int i = 0; i < Output.Length; i++)
            {
                Output[i] = 0;
            }
        }

        private bool TryCreateValidFrustum(FrustumPlane parent, out FrustumPlane result,
            Slope left, Slope bottom, Slope right, Slope up, FrustumPlaneType type, bool isNextPlane)
        {
            result = new FrustumPlane()
            {
                TruncateY = parent.TruncateY + Convert.ToInt32(isNextPlane),
                Type = type,
                TransformId = parent.TransformId,
                StartZY = bottom,
                EndZY = up,
                StartXY = left,
                EndXY = right,
            };

            bool isValid = result.IsValid();
            result.Type = isValid ? type : FrustumPlaneType.InvalidOrComplited;
            return isValid;
        }

        private FrustumPlane TruncateFrustum(FrustumPlane parent,
            Slope left, Slope bottom, Slope right, Slope up, FrustumPlaneType type)
        {
            parent.StartZY = bottom;
            parent.EndZY = up;
            parent.StartXY = left;
            parent.EndXY = right;
            parent.Type = parent.IsValid() ? type : FrustumPlaneType.InvalidOrComplited;
            return parent;
        }

        // TODO: add comments
        private void ScanFrustum(FrustumPlane currentFrustum)
        {
            FrustumPlane upper = default, left = default, bottom = default;
            int3 delta = default;
            float viewRange = viewRadius;
            FrustumTransform transform = frustumAreaTransforms[currentFrustum.TransformId];
            bool isFloorAsRoof = transform.zz < 0;

            float startFade = viewRange * START_FADE;
            float fadeRange = viewRange - startFade;

            for (delta.y = currentFrustum.TruncateY; delta.y <= viewRadius; delta.y++)
            {
                currentFrustum.TruncateY = delta.y;

                float fdy = delta.y * 2.0F + 1.0F;  // Futher delta y
                float cdy = delta.y * 2.0F - 1.0F;  // Closer delta y
                int endZ = currentFrustum.EndZ;

                delta.z = currentFrustum.StartZ;
                if (currentFrustum.SkipFirstRowsCount > 0)
                {
                    delta.z += currentFrustum.SkipFirstRowsCount;
                    currentFrustum.SkipFirstRowsCount = 0;
                }

                for (; delta.z <= endZ; delta.z++)
                {
                    bool onBottomEdge = delta.z == 0;
                    bool onTopEdge = delta.z == delta.y;

                    float fdz = delta.z * 2.0F + 1.0F;  // Futher delta z
                    float cdz = delta.z * 2.0F - 1.0F;  // Closer delta z
                    int endX = currentFrustum.EndX;

                    delta.x = currentFrustum.StartX;
                    if (currentFrustum.SkipFirstCellsCount > 0)
                    {
                        delta.x += currentFrustum.SkipFirstCellsCount;
                        currentFrustum.SkipFirstCellsCount = 0;
                    }

                    var previousCellState = CellState.Empty;
                    bool endedOnInvisibleRoof = false;

                    for (; delta.x <= endX; delta.x++)
                    {
                        int3 transformedDelta = transform.DoTransform(delta);
                        if (!bounds.Contains(transformedDelta.x, transformedDelta.y, transformedDelta.z))
                        {
                            continue;
                        }

                        float distanceSqr = delta.x * delta.x + delta.y * delta.y + delta.z * delta.z;
                        if (distanceSqr > viewRange * viewRange)
                        {
                            continue;
                        }

                        var arrayPosition = transformedDelta + arrayCenter;
                        int index = Utilites.EncodeInt3(arrayPosition, size);
                        var cellData = input[index];

                        float roofTransparency = 1.0F;

                        // We accept that if cell with opaque wall, its have roof anyway
                        if (cellData.CellTransparency == 0.0F)
                        {
                            roofTransparency = 0.0F;
                        }
                        else
                        {
                            // We receive data with floor transparency. Floor for current z level is
                            // roof for previous z level. So we check if up and down inverted or not in
                            // current frustum transform.
                            if (isFloorAsRoof)
                            {
                                roofTransparency = cellData.FloorTransparency;
                            }
                            else
                            {
                                // If above cell out of bounds, we accept that roof is fully transparent
                                if (transformedDelta.z < bounds.maxZ - 1)
                                {
                                    var aboveCellPosition = arrayPosition;
                                    aboveCellPosition.z++;
                                    int aboveCellIndex = Utilites.EncodeInt3(aboveCellPosition, size);
                                    var aboveCell = input[aboveCellIndex];
                                    roofTransparency = aboveCell.FloorTransparency;
                                }
                            }
                        }

                        var currentCellState = GetState(cellData.CellTransparency, roofTransparency);

                        Slope bottomRoofOcclusionSlopeZY;

                        // In some cases roof is upper than slope and we don't see roof of cell.
                        // There is no point in handle the cell as if they have a roof in this case.
                        if (currentCellState == CellState.Roof)
                        {
                            bottomRoofOcclusionSlopeZY = new Slope(fdz, fdy);
                            if (bottomRoofOcclusionSlopeZY > currentFrustum.EndZY)
                            {
                                currentCellState = CellState.Empty;
                            }
                        }

                        var transientEvent = GetEvent(previousCellState, currentCellState);

                        if (transientEvent == CellStateTransientEvent.None)
                        {
                            goto ADD_CELL_AND_CONTINUE;
                        }

                        bottomRoofOcclusionSlopeZY = new Slope(fdz, fdy);
                        Slope bottomWallOcclusionSlopeZY = new Slope(cdz, fdy);
                        Slope bottomOcclusionEdgeSlopeZY = currentCellState == CellState.Roof ?
                              bottomRoofOcclusionSlopeZY : bottomWallOcclusionSlopeZY;

                        // Closer delta x
                        float cdx = delta.x * 2.0F - 1.0F;

                        Slope leftOcclusionEdgeSlopeXY = new Slope(cdx, fdy);
                        Slope upperOcclusionEdgeSlope = new Slope(fdz, cdy);

                        if (previousCellState == CellState.Empty)
                        {
                            // If we meet roof or wall, create 3 frustums and save them.
                            // Some of them can be invalid.

                            if (leftOcclusionEdgeSlopeXY > currentFrustum.StartXY)
                            {
                                // left
                                TryCreateValidFrustum(currentFrustum, out left,
                                    currentFrustum.StartXY, Slope.Max(bottomOcclusionEdgeSlopeZY, currentFrustum.StartZY),
                                    leftOcclusionEdgeSlopeXY, Slope.Min(upperOcclusionEdgeSlope, currentFrustum.EndZY),
                                    FrustumPlaneType.Left, true);
                            }

                            if (upperOcclusionEdgeSlope < currentFrustum.EndZY)
                            {
                                // upper
                                TryCreateValidFrustum(currentFrustum, out upper,
                                     currentFrustum.StartXY, upperOcclusionEdgeSlope,
                                     currentFrustum.EndXY, currentFrustum.EndZY,
                                     FrustumPlaneType.Upper, false);

                                // Prevents an infinite loop in some cases after splitting off the upper frustum.
                                // We don't want to recheck the row that just caused the upper frustum to be split off,
                                // since that can lead to an identical span being split off again, hence the
                                // infinite loop.
                                upper.SkipFirstRowsCount = upper.StartZ == currentFrustum.StartZ ? 1 : 0;

                                // In some cases we dont fully handle cell with upper frustum,
                                // need add another visible part that included in left frustum, so we
                                // can use it for mark cell as fully visible
                                if (left.Type != FrustumPlaneType.InvalidOrComplited &&
                                    upper.Type != FrustumPlaneType.InvalidOrComplited)
                                {
                                    left.SkipFirstRowsCount = left.EndZ - left.StartZ;
                                    left.TruncateY--;
                                }
                            }

                            if (bottomOcclusionEdgeSlopeZY > currentFrustum.StartZY)
                            {
                                // bottom
                                TryCreateValidFrustum(currentFrustum, out bottom,
                                    currentFrustum.StartXY, currentFrustum.StartZY,
                                    currentFrustum.EndXY, Slope.Min(bottomOcclusionEdgeSlopeZY, currentFrustum.EndZY),
                                    FrustumPlaneType.Bottom, true);
                            }
                        }

                        if (transientEvent == CellStateTransientEvent.FromRoofToWall)
                        {
                            // Convert bottom frustum to left frustum if bottom is valid
                            // and immediately scan it - we need assigne new bottom frustum with lower
                            // endZY
                            if (bottom.Type != FrustumPlaneType.InvalidOrComplited)
                            {
                                bottom = TruncateFrustum(bottom,
                                    bottom.StartXY, Slope.Max(bottomWallOcclusionSlopeZY, currentFrustum.StartZY),
                                    leftOcclusionEdgeSlopeXY, bottom.EndZY,
                                    FrustumPlaneType.Left);

                                if (bottom.Type != FrustumPlaneType.InvalidOrComplited)
                                {
                                    ScanFrustum(bottom);
                                }

                                // Create new bottom instead bottom that was created before
                                TryCreateValidFrustum(currentFrustum, out bottom,
                                    currentFrustum.StartXY, currentFrustum.StartZY,
                                    currentFrustum.EndXY, Slope.Min(bottomOcclusionEdgeSlopeZY, currentFrustum.EndZY),
                                    FrustumPlaneType.Bottom, true);
                            }
                        }
                        else if (transientEvent == CellStateTransientEvent.FromWallToRoof)
                        {
                            // Truncate bottom that created from wall
                            // and immediately scan it - we need assigne new bottom frustum
                            Slope previousCellRightOcclusionSlopeXY = new Slope(cdx, cdy);
                            if (bottom.Type != FrustumPlaneType.InvalidOrComplited)
                            {
                                bottom = TruncateFrustum(bottom,
                                    bottom.StartXY, bottom.StartZY,
                                    previousCellRightOcclusionSlopeXY, bottomWallOcclusionSlopeZY,
                                    FrustumPlaneType.Bottom);

                                if (bottom.Type != FrustumPlaneType.InvalidOrComplited)
                                {
                                    ScanFrustum(bottom);
                                }
                            }

                            // Set current start XY
                            currentFrustum.StartXY = previousCellRightOcclusionSlopeXY;

                            // Create new frustrum as new bottom
                            TryCreateValidFrustum(currentFrustum, out bottom,
                                currentFrustum.StartXY, currentFrustum.StartZY,
                                currentFrustum.EndXY, bottomRoofOcclusionSlopeZY,
                                FrustumPlaneType.Bottom, true);
                        }

                        if (currentCellState == CellState.Empty)
                        {
                            // If previous cell was a with roof, we assign botton as new current,
                            // that will running scan parallel with current frustum.

                            if (transientEvent == CellStateTransientEvent.FromRoofToEmpty &&
                                bottom.Type != FrustumPlaneType.InvalidOrComplited)
                            {
                                bottom.TruncateY--;
                                bottom.SkipFirstRowsCount = delta.z - currentFrustum.StartZ;
                                bottom.SkipFirstCellsCount = delta.x - currentFrustum.StartX;
                            }

                            // When we completed block, truncate current frustum and
                            // start scan all left frustums

                            Slope previousCellRightOcclusionSlopeXY = new Slope(cdx, cdy);
                            Slope previousCellBottomOcclusionSlopeZY = previousCellState == CellState.Roof ?
                                bottomRoofOcclusionSlopeZY : bottomWallOcclusionSlopeZY;

                            var startXY = Slope.Max(previousCellRightOcclusionSlopeXY, currentFrustum.StartXY);
                            var endZY = Slope.Min(upperOcclusionEdgeSlope, currentFrustum.EndZY);
                            var startZY = Slope.Max(previousCellBottomOcclusionSlopeZY, currentFrustum.StartZY);

                            currentFrustum = TruncateFrustum(currentFrustum,
                                startXY, startZY, currentFrustum.EndXY, endZY,
                                FrustumPlaneType.CurrentRight);

                            ScanValidFrustums(ref upper, ref bottom, ref left);
                        }

                    ADD_CELL_AND_CONTINUE:
                        // If current frustum now invalid, start other frustums scans and close current
                        if (currentFrustum.Type == FrustumPlaneType.InvalidOrComplited)
                        {
                            ScanValidFrustums(ref upper, ref bottom, ref left);
                            return;
                        }

                        float visibility = CalcCellVisibility(delta, currentFrustum,
                            currentCellState, previousCellState);

                        float distance = Mathf.Sqrt(distanceSqr);
                        //Decrease in visibility from distance
                        if (distance > startFade)
                        {
                            float k = Mathf.SmoothStep(1.0F, 0.0F, (distance - startFade) / fadeRange);
                            visibility *= k;
                        }

                        // if cell on edge its can be handled by other thread, so
                        // we use special list for avoid race condition in this case
                        bool onLeftEdge = delta.x == 0;
                        bool onRightEdge = delta.x == delta.y;
                        if (onLeftEdge || onBottomEdge || onRightEdge || onTopEdge)
                        {
                            // Encode x, y and z to integer
                            // Maybe here's no reason do it, but integer faster than int3 so why not?
                            int encoded = Utilites.EncodeInt3(arrayPosition, size);
                            ref var edgeOutput = ref edgesOutput[currentFrustum.TransformId];
                            for (int i = 0; i < edgeOutput.Length; i++)
                            {
                                // If key == -1 its free for use
                                if (edgeOutput[i].Key == -1)
                                {
                                    edgeOutput[i] = new KeyValuePair<int, float>(encoded, visibility);
                                    break;
                                }

                                if (i == edgeOutput.Length - 1)
                                {
                                    Debug.LogWarning("Out of array bounds for edge cells! Recheck size formula");
                                }
                            }
                        }
                        else
                        {
                            // cells that fully included in initial frustum can be handled
                            // parallel for each initial frustum thread
                            Output[index] += visibility;
                        }

                        previousCellState = currentCellState;
                    }

                    // if row ended with roof or wall, scan
                    // frustums that left and close current frustum
                    ScanValidFrustums(ref upper, ref bottom, ref left);
                    if (previousCellState != CellState.Empty && !endedOnInvisibleRoof)
                    {
                        return;
                    }
                }
            }
        }

        private CellState GetState(float cellTransparency, float roofTransparency)
        {
            if (cellTransparency == 0)
            {
                return CellState.Wall;
            }
            else
            {
                if (roofTransparency == 0)
                {
                    return CellState.Roof;
                }
                else
                {
                    return CellState.Empty;
                }
            }
        }

        private CellStateTransientEvent GetEvent(CellState previousState, CellState currentState)
        {
            switch (previousState)
            {
                case CellState.Empty:
                    switch (currentState)
                    {
                        case CellState.Roof: return CellStateTransientEvent.FromEmptyToRoof;
                        case CellState.Wall: return CellStateTransientEvent.FromEmptyToWall;
                        default: return CellStateTransientEvent.None;
                    }

                case CellState.Roof:
                    switch (currentState)
                    {
                        case CellState.Empty: return CellStateTransientEvent.FromRoofToEmpty;
                        case CellState.Wall: return CellStateTransientEvent.FromRoofToWall;
                        default: return CellStateTransientEvent.None;
                    }

                case CellState.Wall:
                    switch (currentState)
                    {
                        case CellState.Roof: return CellStateTransientEvent.FromWallToRoof;
                        case CellState.Empty: return CellStateTransientEvent.FromWallToEmpty;
                        default: return CellStateTransientEvent.None;
                    }

                default: return CellStateTransientEvent.None;
            }
        }

        // Start scan all current saved frustums if its valid
        private void ScanValidFrustums(ref FrustumPlane upper, ref FrustumPlane bottom, ref FrustumPlane left)
        {
            if (upper.Type != FrustumPlaneType.InvalidOrComplited) ScanFrustum(upper);
            if (left.Type != FrustumPlaneType.InvalidOrComplited) ScanFrustum(left);
            if (bottom.Type != FrustumPlaneType.InvalidOrComplited) ScanFrustum(bottom);
            upper.Type = FrustumPlaneType.InvalidOrComplited;
            left.Type = FrustumPlaneType.InvalidOrComplited;
            bottom.Type = FrustumPlaneType.InvalidOrComplited;
        }

        // TODO: calc visible volume when closer point and futher point not inside one cell more accurate
        // TODO: handle cases when two slopes pass thought same cell
        private float CalcCellVisibility(int3 delta, FrustumPlane frustum, CellState state, CellState previousState)
        {
            // Note that frustum slopes does not need exactly pass through cell making other sub-frustum.
            // On picture case its sub-frustum. Its can be also sloped (or not sloped) prism, so we use different formules for
            // calculating visible volume. Usually cells just included into volume of frustum.
            //
            // View from above
            //
            //            futherWidth
            //               <---->
            //   futherStart |    | futherEnd
            //               |    |
            //            +--o----o+
            //            |##|   /#|                ↑ y axis
            //            |#|    |#|                → x axis
            //            |#/   /##|
            //            +o---o---+                # - invisible
            //             |   |                    o - points created by slope plane when its crossing cell
            // closerStart |   | closerEnd
            //             <--->
            //          closerWidth
            //
            // On this picture frustum goes along axis O1 to O, from up to down, AD - x axis, AB - z axis, O1O - y axis
            // https://8b08ab88-ee1b-4b04-9ae9-321e0da71ae2.selcdn.net/e1bfb4f4-807b-4424-88b6-0a0f5cb9af6a/Dsk_06.png

            float exactStartXCloser = frustum.ExactStartXCloser;
            float exactStartXFuther = frustum.ExactStartXFuther;
            float exactStartZCloser = frustum.ExactStartZCloser;
            float exactStartZFuther = frustum.ExactStartZFuther;

            float exactEndXCloser = frustum.ExactEndXCloser;
            float exactEndXFuther = frustum.ExactEndXFuther;
            float exactEndZCloser = frustum.ExactEndZCloser;
            float exactEndZFuther = frustum.ExactEndZFuther;

            float closerWidth = CalcVisibilityOfCellEdge(exactStartXCloser, exactEndXCloser, delta.x);
            float closerHeight = CalcVisibilityOfCellEdge(exactStartZCloser, exactEndZCloser, delta.z);
            float areaCloser = closerWidth * closerHeight;

            float futherWidth = CalcVisibilityOfCellEdge(exactStartXFuther, exactEndXFuther, delta.x);
            float futherHeight = CalcVisibilityOfCellEdge(exactStartZFuther, exactEndZFuther, delta.z);
            float areaFuther = futherWidth * futherHeight;

            float visibility = 0;

            // UNDONE: handle opaque floor
            if (state == CellState.Wall)
            {
                // If we have opaque cell,  we calc visibility as visible area on visible sides of cell.
                // We need to think about how to consider the cells to be 100 % visible,
                // because in reality we will never see the cube from all sides.
                // However, we can see it from 3 sides. But let skip floor or roof side,
                // maybe will handle it separately.
                // 2 sides left. Hovever in most cases wall have another walls on neigbour sides so it looks not
                // good when we see just 50% of wall, when side area blocked by another wall on previous cell.
                // So let say wall/opaque cell is visible 100% if we fully see at least 1 side.
                // On picute visible sides areas marked by 'x'.
                //
                //       ---+-----+---
                //          o#####|   
                //         /x#####|   
                //       -/-xxxxo-+---
                //       /  | _/  |   
                //      /  _|/    |   
                //     / _/ +-----+---
                //    @_/

                if (previousState == CellState.Wall)
                {
                    return areaCloser;
                }

                if (Mathf.FloorToInt(exactStartXCloser) != Mathf.FloorToInt(exactEndXFuther))
                {
                    float depthEnd = frustum.StartXY.DeltaValue == 0 ?
                        float.PositiveInfinity :
                        frustum.StartXY.CalcY(delta.x - 0.5F) + 0.5F;

                    float depthStart = frustum.EndXY.CalcY(exactEndXCloser - 0.5F) + 0.5F;
                    float closerDepth = CalcVisibilityOfCellEdge(depthStart, depthEnd, delta.y);

                    float sideArea = closerDepth * ((closerHeight + futherHeight) / 2);

                    return Mathf.Clamp(sideArea + areaCloser, 0.0F, 1.0F);
                }
                else
                {
                    return areaCloser;
                }
            }

            // Short check for most likely possible case. Used for increase performance (but not tested).
            if (areaCloser == 1.0F && areaFuther == 1.0F)
            {
                visibility = 1.0F;
            }
            else if (closerWidth == futherWidth)
            {
                // Prism (https://www.meetoptics.com/docs/webp/eksmaoptics/62b9354685d5d074cc1206336f4eebdf.webp)
                visibility += (futherHeight + closerHeight) / 2 * closerWidth;
            }
            else if (closerHeight == futherHeight)
            {
                // Prism (https://www.meetoptics.com/docs/webp/eksmaoptics/62b9354685d5d074cc1206336f4eebdf.webp)
                visibility += (futherWidth + closerWidth) / 2 * closerHeight;
            }
            else if (areaCloser == 0 || areaFuther == 0)
            {
                // Sloped prism 

                //TODO: combine formulas

                // We calculating sloped prism volume by add two parts:
                // 1) Pyramid: 1/3 * S * h
                // (https://media.geeksforgeeks.org/wp-content/uploads/20221011103505/vrp.jpg)
                // 2) Tetrahedron: 1/6 * a * b * d * sin(∠ab) (where sin(∠ab) = 1, d = h)
                // (http://school-collection.edu.ru/dlrstore-wrapper/41cad97f-3245-422f-b97f-30dddea784f0/Obem_tetraedra_clip_image008.gif)

                float h = 1;

                if (closerWidth == 0 || closerHeight == 0)
                {
                    visibility += 1.0F / 3.0F * futherWidth * futherHeight * h;
                }
                else if (futherWidth == 0 || futherHeight == 0)
                {
                    visibility += 1.0F / 3.0F * closerWidth * closerHeight * h;
                }

                if (closerWidth <= 0 && closerHeight > 0 || futherHeight <= 0 && futherWidth > 0)
                {
                    visibility += 1.0F / 6.0F * closerHeight * futherWidth * h;
                }
                else if (closerHeight <= 0 && closerWidth > 0 || futherWidth <= 0 && futherHeight > 0)
                {
                    visibility += 1.0F / 6.0F * closerWidth * futherHeight * h;
                }
            }
            else
            {
                // Frustum volume
                visibility += 1.0F / 3.0F * (areaCloser + areaFuther + Mathf.Sqrt(areaCloser * areaFuther));
            }

            return visibility;
        }

        private float CalcVisibilityOfCellEdge(float start, float end, int position)
        {
            //  Possible case
            //                      end cell edge
            //         other cell          |    other cell
            //               V             V      V 
            //          x|xxxxxxxx|xxxA----|--------|----Bxxx|x
            //                    ^  /                __/
            // start cell edge___/  /              __/
            //                     /            __/
            //                    ^          __/   <---- end frustum plane
            //      start frustum plane     /
            //
            // A - start of visible part, B - end of visible part, x - invisible part, dash - visible part

            float startCellEdge = position;
            float endCellEdge = position + 1;

            // Delta will be less than zero if the truncate plane pass through cells
            // Thats lenght of invisible part of edge
            float deltaStart = startCellEdge - start;
            float deltaEnd = end - endCellEdge;

            // if delta > 0, we fully sees edge of cell.
            // if delta < -1, edge fully invisible.

            deltaStart = Mathf.Clamp(deltaStart, -1, 0);
            deltaEnd = Mathf.Clamp(deltaEnd, -1, 0);

            // Visible edge lenght = 1 - invisible parts
            float visibleCellEdgePart = 1 + (deltaStart + deltaEnd);

            // if edge lenght < 0 edge fully invisible and some part of cell
            // inviviblie too
            return visibleCellEdgePart;
        }

        public struct InputCellData
        {
            public float CellTransparency;
            public float FloorTransparency;
        }

        // https://i.stack.imgur.com/Mx70m.png
        private struct FrustumPlane
        {
            public int TransformId;
            public int TruncateY;
            public int SkipFirstRowsCount;
            public int SkipFirstCellsCount;
            public FrustumPlaneType Type;

            // TODO: Remove me after debug
            // public int Id;

            public Slope StartZY;
            public Slope EndZY;
            public Slope StartXY;
            public Slope EndXY;

            public float ExactStartXCloser => StartXY.CalcValue(TruncateY - 0.5F) + 0.5F;
            public float ExactEndXCloser => EndXY.CalcValue(TruncateY - 0.5F) + 0.5F;
            public float ExactStartZCloser => StartZY.CalcValue(TruncateY - 0.5F) + 0.5F;
            public float ExactEndZCloser => EndZY.CalcValue(TruncateY - 0.5F) + 0.5F;

            public float ExactStartXFuther => StartXY.CalcValue(TruncateY + 0.5F) + 0.5F;
            public float ExactEndXFuther => EndXY.CalcValue(TruncateY + 0.5F) + 0.5F;
            public float ExactStartZFuther => StartZY.CalcValue(TruncateY + 0.5F) + 0.5F;
            public float ExactEndZFuther => EndZY.CalcValue(TruncateY + 0.5F) + 0.5F;

            public int StartX => Mathf.FloorToInt(ExactStartXCloser);
            public int EndX => Mathf.RoundToInt(ExactEndXCloser);
            public int StartZ => Mathf.FloorToInt(ExactStartZCloser);
            public int EndZ => Mathf.RoundToInt(ExactEndZCloser);

            public bool IsValid()
            {
                // Accept slope is valid
                return
                    // If start less (left slop) than end (right slop)
                    EndZY > StartZY && EndXY > StartXY &&
                    // and if angle between y axis and slope more than 0 degree (x and z dont going left or bottom)
                    StartZY.DeltaValue >= 0 && StartXY.DeltaValue >= 0 &&
                    // and if angle between y axis and slope less than 45 degree (y grows faster than value or same)
                    EndXY.DeltaY >= EndXY.DeltaValue && EndZY.DeltaY >= EndZY.DeltaValue;
            }

            //public override string ToString()
            //{
            //    StringBuilder sb = new StringBuilder();
            //    //sb.Append($"{nameof(Id)}: {Id}\n");
            //    sb.Append($"{nameof(TransformId)}: {TransformId}\n");
            //    sb.Append($"{nameof(Type)}: {Type}\n");
            //    sb.Append($"{nameof(TruncateY)}: {TruncateY}\n");

            //    return sb.ToString();
            //}
        }

        // Convert coeffs for transform frustum coords to wrold space 
        private readonly struct FrustumTransform
        {
            public readonly sbyte xx;
            public readonly sbyte yx;
            public readonly sbyte xy;
            public readonly sbyte yy;
            public readonly sbyte zz;

            public readonly byte Id;

            public readonly float StartDegreeX;
            public readonly float EndDegreeX;

            public FrustumTransform(sbyte xx, sbyte yx, sbyte xy, sbyte yy, sbyte zz,
                float startDegreeX, float endDegreeX, byte id)
            {
                this.xx = xx;
                this.yx = yx;
                this.xy = xy;
                this.yy = yy;
                this.zz = zz;

                Id = id;
                StartDegreeX = startDegreeX;
                EndDegreeX = endDegreeX;
            }

            public int3 DoTransform(int3 point)
            {
                int3 transformed = default;
                transformed.x = point.x * xx + point.y * yx;
                transformed.y = point.x * xy + point.y * yy;
                transformed.z = point.z * zz;
                return transformed;
            }
        }

        private readonly struct Slope
        {
            //     start      end
            //       |      _/
            //       |    _/ 
            //       |  _/   
            //       | /
            //       @    <- viewPoint

            // Slope represents slope plane of frustum. Actually its sloped plane.
            // https://nekin.info/math/figs/m0413-1.png

            public readonly float DeltaY;
            public readonly float DeltaValue;
            public readonly float K;

            public Slope(float dv, float dy)
            {
                DeltaValue = Math.Abs(dv);
                DeltaY = dv < 0 ? -dy : dy;
                K = DeltaY / DeltaValue;
            }

            public float CalcValue(float y)
            {
                return y / K;
            }

            public float CalcY(float value)
            {
                return value * K;
            }

            public static bool operator <(Slope lhs, Slope rhs)
            {
                if ((float.IsInfinity(lhs.K) || lhs.K < 0.0F) && rhs.K < float.PositiveInfinity && rhs.K > 0.0F)
                {
                    return true;
                }
                else if (lhs.K > 0.0F && rhs.K > 0.0F)
                {
                    return lhs.K > rhs.K;
                }
                else if (lhs.K < 0.0F && rhs.K < 0.0F)
                {
                    return lhs.K < rhs.K;
                }

                return false;
            }
            public static bool operator >(Slope lhs, Slope rhs)
            {
                return rhs < lhs;
            }
            public static bool operator ==(Slope lhs, Slope rhs)
            {
                return lhs.K == rhs.K;
            }
            public static bool operator !=(Slope lhs, Slope rhs)
            {
                return lhs.K != rhs.K;
            }
            public static bool operator >=(Slope lhs, Slope rhs)
            {
                return lhs > rhs || lhs == rhs;
            }
            public static bool operator <=(Slope lhs, Slope rhs)
            {
                return lhs < rhs || lhs == rhs;
            }
            public static Slope Max(Slope a, Slope b)
            {
                return a > b ? a : b;
            }
            public static Slope Min(Slope a, Slope b)
            {
                return a < b ? a : b;
            }

            public override bool Equals(object obj)
            {
                return obj is Slope slope && K == slope.K;
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(DeltaY, DeltaValue, K);
            }
        }
    }
}
