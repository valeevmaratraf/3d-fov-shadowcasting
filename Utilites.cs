using System;
using Unity.Mathematics;

namespace Varafmar.AdAstra
{
    public static class Utilites
    {
        public static int3 DecodeInt3(int index, int3 size)
        {
            int perY = size.x * size.z;
            int y = index / perY;
            int encodedYPart = perY * y;

            int x = (index - encodedYPart) / size.z;
            int encodedXPart = size.z * x;

            int z = index - encodedYPart - encodedXPart;

            return new int3(x, y, z);
        }

        public static int EncodeXYZ(int x, int y, int z, int3 size)
        {
            return y * size.x * size.z + x * size.z + z;
        }

        public static int EncodeInt3(int3 target, int3 size) => EncodeXYZ(target.x, target.y, target.z, size);
		
		public static float FloorMidpoint(float value, int midpointsCount)
        {
            value *= midpointsCount;
            return Mathf.Floor(value) / midpointsCount;
        }
    }
}
