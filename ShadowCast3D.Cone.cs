using UnityEngine;

namespace Varafmar.AdAstra
{
    public partial class ShadowCast3D
    {
        public void ComputeVisibilityCone(float viewRadius, float viewDirection, float viewAngle)
        {
            viewAngle = Mathf.Clamp(viewAngle, 0.0F, 360.0F);

            if (viewAngle == 360.0F)
            {
                ComputeVisibility360(viewRadius);
                return;
            }

            if (viewAngle == 0.0F)
            {
                Output[arrayCenterIndex] = 1.0F;
                return;
            }

            viewDirection %= 360.0F;

            float halfViewAngle = viewAngle / 2;
            float startAngle = viewDirection - halfViewAngle;
            float endAngle = viewDirection + halfViewAngle;

            bool cleanUp = true;

            if (endAngle > 360.0F)
            {
                ScanConeClamped(viewRadius, 0.0F, endAngle - 360.0F, ref cleanUp);
                cleanUp = false;
                endAngle = 360.0F;
            }

            if (startAngle < 0.0F)
            {
                ScanConeClamped(viewRadius, 360.0F + startAngle, 360.0F, ref cleanUp);
                cleanUp = false;
                startAngle = 0.0F;
            }

            ScanConeClamped(viewRadius, startAngle, endAngle, ref cleanUp);
        }

        private void ScanConeClamped(float viewRadius, float startAngle, float endAngle, ref bool cleanUp)
        {
            for (int i = 0; i < frustumAreaTransforms.Length; i++)
            {
                var fr = defaultInitialFrustum;
                var tr = frustumAreaTransforms[i];
                fr.TransformId = i;

                bool isInvertedDirection = tr.Id % 2 == 1;

                var startSlope = AngleToTransformedSlope(tr, startAngle, isInvertedDirection);
                var endSlope = AngleToTransformedSlope(tr, endAngle, isInvertedDirection);

                fr.StartXY = isInvertedDirection ? endSlope : startSlope;
                fr.EndXY = isInvertedDirection ? startSlope : endSlope;
                if (!fr.IsValid())
                {
                    fr.Type = FrustumPlaneType.InvalidOrComplited;
                }

                initialFrustums[i] = fr;
            }

            ComputeVisibilityInternal(viewRadius, cleanUp);
            cleanUp = false;
        }

        private Slope AngleToTransformedSlope(FrustumTransform tr, float angle, bool isInvertedDirection)
        {
            bool xySwapped = tr.yx != 0 || tr.xy != 0;
            float k = Mathf.Tan(angle * Mathf.Deg2Rad);

            //https://i.ytimg.com/vi/bTznWsZL38w/maxresdefault.jpg

            if (angle <= tr.StartDegreeX)
            {
                k = isInvertedDirection ? 1.0F : float.PositiveInfinity;
            }
            else if (angle >= tr.EndDegreeX)
            {
                k = isInvertedDirection ? float.PositiveInfinity : 1.0F;
            }
            else
            {
                k = xySwapped ? 1.0F / k : k;
            }

            k = Mathf.Abs(k);

            float dx = 1000.0F;
            float dy = dx * k;
            return new Slope(dx, dy);
        }
    }
}
